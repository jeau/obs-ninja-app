# OBS Ninja Cloudron App

This repository contains the Cloudron app package source for [OBS Ninja](https://obs.ninja/).


## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd obs-ninja-app

cloudron build
cloudron install
```

