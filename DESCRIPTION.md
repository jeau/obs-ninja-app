This app packages OBS.Ninja <upstream>17.1</upstream>.

## About

OBS.Ninja brings live video from a smartphone, tablet, remote computer, or friends devices directly into OBS Studio or other compatible broadcasting software. It uses cutting edge Peer-to-Peer forwarding technology that offers privacy and ultra-low latency. it's also available as customizable and deployable open-source code. The Swiss Army knife of audio and video transmission.
