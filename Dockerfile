FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code

# OBS Ninja version
ARG VERSION=17.1

# install dependencies
RUN apt-get update && apt-get install -y rsync 

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log
COPY apache/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf

# download and install OBS Ninja version
RUN wget https://github.com/steveseguin/obsninja/archive/refs/tags/v${VERSION}.zip \
    -O /tmp/obsninja.zip && \
    unzip /tmp/obsninja.zip -d /tmp 

RUN rsync -r /tmp/obsninja-${VERSION}/ /app/code/
RUN rm -Rf /tmp/obsninja*

# add code
COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
